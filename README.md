# micropython-tm1638spi

Micropython Library for a popular board with 8 7-segment digits,
8 separate LEDs and 8 push buttons controlled by a TM1638.

Author: Rob Hamerling, Copyright (c) 2023..2023 all rights reserved.

Sample tested with a PyBoard 1.1, ESP32, Raspberry Pi Pico.

Class TM1638SPI(spi, cs, brightness=7)
   - spi: instance of active SPI module (for wiring see comments in library)
   - cs: Pin number of CS (Chip Select) signal (connected to STB of TM1638)
   - brightness: value in range 0..7

The script 'tm1638test.py' shows examples of use:
   - Reset
   - Brightness
   - ASCII display
   - Set a single segment
   - Hex digits
   - Text String with Decimal point
   - TEXT + ASCII combo
   - Integer Decimal number
   - Text String + Float
   - Text String + decimal number
   - Multiple dots
   - Display Overflow
   - Scrolling text
   - All LEDS one-by-one
   - Buttons + LEDS
