"""
    Elementary program to test a TM1638 board with (hardware) SPI
    with 8 7-segment displays, 8 LEDs and 8 buttons

    test 0 Reset
    test 1 Brightness
    test 2 ASCII display
    test 3 Set a single segment
    test 4 Hex digits
    test 5 Text String with Decimal point
    test 6 TEXT + ASCII combo
    test 7 Integer Decimal number
    test 8 Text String + Float
    test 9 Text String + decimal number
    test 10 Multiple dots
    test 11 Display Overflow
    test 12 Scrolling text
    test 13 All LEDS one-by-one
    test 14 Buttons + LEDS
"""

from micropython import const
from os import uname
from sys import exit, implementation
from machine import Pin, SPI
from tm1638spi import TM1638spi     # class supportint the TM1638 module
from time import sleep, sleep_ms

#  Some constant for the tests
testDelayShort = const(1000)
testDelayLong  = const(5000)


def test0(tm):
    print("test 0:  Show third LED")
    tm.setLED(2, True)

def test1(tm):
    print("test 1:  Brightness and clear")
    tm.displayText("00000000")
    for i in range(8):
        tm.brightness(i)
        sleep_ms(testDelayShort)
    tm.brightness(2)

def test2(tm):
    print("test 2:  char-by-char 2.348")
    tm.displayCharwDot(0, '2')
    sleep_ms(testDelayShort)
    tm.displayChar(1, '3')
    sleep_ms(testDelayShort)
    tm.displayChar(2, '4')
    sleep_ms(testDelayShort)
    tm.displayChar(3, '8')

def test3(tm):
    print("test 3:  Single segments (pos, (dp)gfedcba)")
    #  segment g (middle dash) of digit position 7
    tm.display7Seg(0, 0b10000000)
    tm.display7Seg(1, 0b01000000)
    tm.display7Seg(2, 0b00100000)
    tm.display7Seg(3, 0b00010000)
    tm.display7Seg(4, 0b00001000)
    tm.display7Seg(5, 0b00000100)
    tm.display7Seg(6, 0b00000010)
    tm.display7Seg(7, 0b00000001)

def test4(tm):
    print("test 4a: Hex digits 0..7")
    tm.displayHex(0x01234567)
    sleep_ms(testDelayLong)
    print("test 4b: Hex digits 8..F")
    tm.displayHex(0x89ABCDEF)

def test5(tm):
    print("test 5:  Text with 'decimal' points (abc.d.efgh)")
    #  abcdefgh with decimal point for c and d
    tm.displayText("abc.d.efgh")

def test6(tm):
    print("test 6:  Text + separate digits (ADC=.2.548)")
    #  ADC=.2.548
    tm.displayText("ADC=.")
    tm.displayCharwDot(4, '2')
    sleep_ms(testDelayShort)
    tm.displayChar(5, '5')
    sleep_ms(testDelayShort)
    tm.displayChar(6, '4')
    sleep_ms(testDelayShort)
    tm.displayChar(7, '8')

def test7(tm):
    print("test 7a: Integer no leading zeroes, right aligned  (345)")
    tm.displayDec(345, False, True)
    sleep_ms(testDelayLong)
    print("test 7b: Integer no leading zeroes, left aligned (2345)")
    tm.displayDec(2345, False)
    sleep_ms(testDelayLong)
    print("test 7c: Integer with leading zeroes (00012345)")
    tm.displayDec(12345, True)
    sleep_ms(testDelayLong)
    print("test 7d: Two short numbers (123,678), leading zeroes (01230678)")
    tm.displayDecNibble(123, 678, True)
    sleep_ms(testDelayLong)
    tm.clear()
    print("test 7e: Two short numbers (123,678), no leading zeroes ( 123 678)")
    tm.displayDecNibble(123, 678, False, True)

def test8(tm):
    print("test 8:  Formatted text + integer AdC=.0234")
    workStr = bytearray(11)
    data = 234
    workStr = "ADC=.{:04d}".format(data)   #  "ADC=.0234"
    tm.displayText(workStr)

def test9(tm):
    print("test 9:  Formatted text + Float (AdC=.12.45)")
    voltage = 12.45
    workStr = "ADC=.{:2.2f}".format(voltage)
    tm.displayText(workStr)           #  12.45 VOLT

def test10(tm):
    print("test 10a: Multiple dots test (Hello....)")
    tm.displayText("HELLo....")
    sleep_ms(testDelayLong)
    print("test 10b: SOS in morse (...---...)")
    tm.displayText("...---...")       # SOS in morse

def test11(tm):
    print("test 11: Data overflow, should display (12345678)")
    tm.displayText("1234567890abc")

def test12(tm):
    print("test 12: Scrolling text")
    text = "0123456789ABCDEF "
    for i in range(len(text)):              # complete shift-out
        tm.displayText(text[i : i + 8])     # display substring
        sleep_ms(testDelayShort // 2)

def test13(tm):
    print("test 13: LEDS one by one")
    for i in range(8):
        tm.setLED(i, True)
        sleep_ms(testDelayShort // 2)
        tm.setLED(i, False)

def test14(tm):
    print("test 14: Buttons and LED test,")
    print("   ====> press button(s) and show corresponding LED(s)")
    while (True):                              #  forever
        tm.displayText("buttons")
        pattern = tm.buttons()                  #  get button pattern
        # expected pattern: s8s7s6s5s4s3s2s1
        # binstr = ""
        for i in range(8):
            tm.setLED(i, pattern & (0x01 << i))
            # binstr += "1" if pattern & (0x01 << i) else "0"
        # print("buttons:", binstr)
        sleep_ms(500)

def main(t):
    # Handle differences for specific micropython platforms
    nodename = uname().nodename             # determine actual platform
    # ----- SPI for ESP32 -----
    if nodename == "esp32":
        tm = TM1638spi(SPI(2, sck=Pin(18), mosi=Pin(23), miso=Pin(19)), 5)
    elif nodename == "pyboard":
        tm = TM1638spi(SPI("X"), "X5")
    elif nodename == "rp2":
        tm = TM1638spi(SPI(1, sck=Pin(10, Pin.OUT), mosi=Pin(11, Pin.OUT), miso=Pin(12, Pin.IN)), 13)
    else:
        print(f"==> ERR: The platform {nodename} is not supported.")
        exit()

    series = (test0, test1, test2, test3, test4,
              test5, test6, test7, test8, test9,
              test10, test11, test12, test13, test14)
    t = 0
    print("Starting with test", t)
    while t < len(series):
        try:
            series[t](tm)
            input("Press Enter to continue...")
            # sleep_ms(testDelayLong)
            tm.clear()
            t += 1
        except KeyboardInterrupt:
            print("Terminated!")
            break
    tm.clear()

# Argument is number of the test to start with
main(0)

#
