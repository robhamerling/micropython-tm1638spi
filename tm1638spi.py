"""
    MicroPython TM1638 LED display driver for a board
    with 8x 7-segment displays, 8x individual LEDs and 8x pushbuttons
    Using SPI, teted with PyBoard, ESP32, RP2 Pico.

    Rob Hamerling, June 2023

    Special requirements for connecting the TM1638:
    - MISO directly to DIO
    - a series resistor of 1K between MOSI and DIO
    - a pull-up resistor of 10K from DIO to +3.3V

                               +3.3V
                              _|_
                             |   |
                             |10K|
                             |___|
                               |
    RP2      MISO--------------+-------DIO  TM1638
    PyBoard                    |
    ESP32    MOSI---| 1K |-----+


    Note: With the ESP32 only hardware SPI(2) (VSPI) can be used!

"""

from micropython import const
from time import sleep_us
from machine import Pin, SPI

DISPLAY_OFF  = const(0x80)          # display control command (display off)
BRIGHT_CMD   = const(0x88)          # display brightness setting (display on)

WRITE_INCR   = const(0x40)          # incremental address write mode
                                    # note: WRITE_FIXED never used
BUTTON_MODE  = const(0x42)          # button read mode

SEG7_ADDR    = const(0xC0)          # leftmost 7-segment display address: C0
LEDS_ADDR    = const(0xC1)          # leftmost LED address: C1

DISPLAY_SIZE = const(8)             # number of 7-segment displays

SEG7_MASK    = const(0x07)          # limit 7-segment display offset
LEDS_MASK    = const(0x07)          # limit LED offset
KEYS_MASK    = const(0x11)          # key-input mask
BRIGHT_MASK  = const(0x07)          # limit brightness index
HEX_MASK     = const(0x0F)          # limit value of hex digit
DP_MASK      = const(0x80)          # dp mask in 7-segment display

BRIGHTNESS_DEFAULT = const(2)       # startup brightness

BAUDRATE      = const(660000)       # SPI baudrate (will become 656250 on PyBoard)

class TM1638spi(object):
    """ Support for the TM1638 LED display driver."""
    # 7-segment patterns for 0..9, A..Z (= a..z), blank, dash, star, dp, equal-sign, underscore
    #    note: first 16 patterns represent hex digits 0..F  """
    SEGMENTS = bytearray(b'\x3F\x06\x5B\x4F\x66\x6D\x7D\x07\x7F\x6F' +
                         b'\x77\x7C\x39\x5E\x79\x71\x3D\x76\x06\x1E\x76\x38\x55' +
                         b'\x54\x5C\x73\x67\x50\x6D\x78\x1C\x1C\x2A\x76\x6E\x5B' +
                         b'\x00\x40\x63\x80\x48\x08')

    def __init__(self, spi, chip_select, brightness=BRIGHTNESS_DEFAULT):
        self._spi = spi                    # expecting instance of SPI module (hw/sw)
        self._buffer1 = bytearray(1)       # 1-byte  buffer
        self._buffer4 = bytearray(4)       # 4-bytes buffer
        self._brightness = brightness & BRIGHT_MASK    # limit!
        if hasattr(self._spi, "deinit"):   # check if this SPI supports it
            self._spi.deinit()             # reset SPI module
        # self._spi.init(baudrate=BAUDRATE, polarity=1, phase=1, bits=8)
        self._spi.init(polarity=1, phase=1)
        self._diylsb = False               # system has support for firstbit=SPI.LSB
        try:
            self._spi.init(firstbit=SPI.LSB)
        except NotImplementedError:
            print("Info: firstbit=SPI.LSB not supported by SPI driver")
            self._diylsb = True            # use DIY code for firstbit=LSB
        self._cs = Pin(chip_select, Pin.OUT)   # connect to STB on TM1638 board
        self._cs.on()                      # high (chip-select is active low)
        self.clear()                        # clear all TM1638 registers
                                            # (initiates 'permanent' WRITE_INCR too)

    # private methods:

    @micropython.viper
    def _diyrev(self, x : int) -> int:
        # reverse bits in a byte
        x = (x & 0x0F) << 4 | (x & 0xF0) >> 4
        x = (x & 0x33) << 2 | (x & 0xCC) >> 2
        return (x & 0x55) << 1 | (x & 0xAA) >> 1

    def _send_command(self, cmd):
        """ send command (single byte) """
        self._cs.off()
        self._buffer1[0] = cmd if not self._diylsb else self._diyrev(cmd)
        #if self._diylsb:
        #    print("command: {:08b}".format(cmd))
        #    print("_______: {:08b}".format(self._buffer1[0]))
        self._spi.write(self._buffer1)
        sleep_us(1)
        self._cs.on()

    def _send_addr_byte(self, addr, data):
        """ send an address byte and a data byte """
        self._cs.off()
        self._buffer1[0] = addr if not self._diylsb else self._diyrev(addr)
        self._spi.write(self._buffer1)
        sleep_us(1)
        self._buffer1[0] = data if not self._diylsb else self._diyrev(data)
        self._spi.write(self._buffer1)
        sleep_us(1)
        self._cs.on()

    def _read_buttons(self):
        """ read button memory and return an 8-bits pattern with actual button
            positions in sequence 8,7,6,5,4,3,2,1 ('1'-bit means button pressed)
        """
        self._cs.off()
        # self._send_command(BUTTON_MODE)
        self._buffer1[0] = BUTTON_MODE if not self._diylsb else self._diyrev(BUTTON_MODE)
        self._spi.write(self._buffer1)
        sleep_us(1)
        self._spi.readinto(self._buffer4, 0xFF)   # read 4 bytes (while keeping MOSI low)
                                                # not: no bit reversion required here!
        self._cs.on()
        pattern = 0x00                          # preset: all buttons released!
        for i in range(4):
            if self._diylsb:                    # must reverse bits in byte
                self._buffer4[i] = self._diyrev(self._buffer4[i])
            pattern |= ((self._buffer4[i] & KEYS_MASK) << i)   # accumulate button states
        return pattern

    def __enter__(self):
        """ start 'with' block: enable """
        self.clear()

    def __exit__(self):
        """ terminate 'with' block: disable """
        self.clear()
        self._write_command(DISPLAY_OFF)
        if hasattr(self._spi, "deinit"):
            self._spi.deinit()

    def _encode_char(self, char):
        """ convert char (0..9, A..Z, a..z, ' ', '-', '*', '=' to a 7-segment pattern """
        o = ord(char)
        if ord('0') <= o <= ord('9'):
            return self.__class__.SEGMENTS[o - ord('0')]        # 0-9
        elif ord('A') <= o <= ord('Z'):
            return self.__class__.SEGMENTS[o - ord('A') + 10]   # uppercase A-Z
        elif ord('a') <= o <= ord('z'):
            return self.__class__.SEGMENTS[o - ord('a') + 10]   # lowercase a-z
        elif o == ord(' '):
            return self.__class__.SEGMENTS[36] # space
        elif o == ord('-'):
            return self.__class__.SEGMENTS[37] # dash
        elif o == ord('*'):
            return self.__class__.SEGMENTS[38] # star/degrees
        elif o == ord('.'):
            return self.__class__.SEGMENTS[39] # decimal point
        elif o == ord('='):
            return self.__class__.SEGMENTS[40] # equal sign
        elif o == ord('_'):
            return self.__class__.SEGMENTS[41] # underscore
        else:
            return self.__class__.SEGMENTS[36] # space: replacement for unsupported chars
            # raise ValueError("Character out of range: {:d} '{:s}'".format(o, chr(o)))

    def _encode_string(self, string):
        """ Convert a string to a bytearray with 7-segment patterns
            excluding single dots, which are merged with previous char """
        patterns = bytearray(len(string))
        j = 0
        for i in range(len(string)):
            if string[i] == '.' and j > 0 and not (patterns[j-1] & DP_MASK):
                patterns[j-1] |= DP_MASK
                continue
            patterns[j] = self._encode_char(string[i])
            j += 1
        return patterns[:j]                 # can be shorter than original string

    # ------ public methods ------

    def clear(self):
        """ zeroes to all registers """
        self._send_command(WRITE_INCR)     # required here!
        self._cs.off()
        self._buffer1[0] = SEG7_ADDR if not self._diylsb else self._diyrev(SEG7_ADDR)
        self._spi.write(self._buffer1)
        sleep_us(1)
        self._buffer1[0] = 0x00             # no bit reversion required
        for _ in range(16):
            self._spi.write(self._buffer1)
            sleep_us(1)
        self._cs.on()
        self.brightness()                   # (re)set brightness

    def brightness(self, brightness=None):
        """ set the display brightness 0..7
            brightness 0 = 1/16th, 7 = 14/16th duty cycle
            returns actual setting
        """
        if not brightness == None:          # new brightness
            self._brightness = brightness & BRIGHT_MASK  # limit value
        self._send_command(BRIGHT_CMD | self._brightness)
        return self._brightness

    def setLED(self, offset, state):
        """ set the state of a single LED to 0 or 1 """
        self._send_addr_byte(LEDS_ADDR | ((offset & LEDS_MASK) << 1), 1 if not state == 0 else 0)

    def displayDec(self, nbr, leadingzeroes=False, rightaligned=False):
        """ Display a numeric value -9999999 .. 99999999,
            with [optionally] leading zeroes, right aligned.
            Without leading zeroes default: left aligned.
        """
        nbr = max(const(-9999999), min(nbr, const(99999999)))
        if leadingzeroes:
            string = "{:>08d}".format(nbr)
        elif rightaligned:
            string = "{:>8d}".format(nbr)
        else:
            string = "{:<8d}".format(nbr)
        self.displayText(string)

    def displayDecNibble(self, nbr1, nbr2, leadingzeroes=False, rightaligned=False):
        """ display 2 decimal numbers, one on each half of the display
            with [optionally] leading zeroes, right aligned.
            without leading zeroes default left aligned
        """
        num1 = max(const(-999), min(nbr1, const(9999)))
        num2 = max(const(-999), min(nbr2, const(9999)))
        if leadingzeroes:
            string = "{:>04d}{:>04d}".format(nbr1, nbr2)
        elif rightaligned:
            string = "{:>4d}{:>4d}".format(nbr1, nbr2)
        else:
            string = "{:<4d}{:<4d}".format(nbr1, nbr2)
        self.displayText(string)

    def display7Seg(self, offset, pattern):
        """ display a single 7-segment pattern on a relative position """
        self._send_addr_byte(SEG7_ADDR + ((offset & SEG7_MASK) << 1), pattern)

    def displayHexDigit(self, offset, digit):
        """ convert a (hex) digit 0..9, A..F to a segment pattern """
        self.display7Seg(offset, self.__class__.SEGMENTS[digit & HEX_MASK])

    def displayHex(self, val):
        """ display a hex value 0..0xFFFFFFFF, right aligned, leading zeroes """
        string = "{:08x}".format(val & const(0xFFFFFFFF))
        self.displayText(string)

    def displayChar(self, offset, ascii):
        """ display ASCII character on a relative position """
        self.display7Seg(offset, self._encode_char(ascii))

    def displayCharwDot(self, offset, ascii):
        """ display char with decimal point segment on a relative position """
        self.display7Seg(offset, self._encode_char(ascii) | DP_MASK)

    def displaypatterns(self, patterns, offset=0):
        """ display a bytearray of 7-segment patterns starting at a relative position
            truncate output beyond display """
        offset &= SEG7_MASK                 # set limit
        for pattern in patterns:
            self._send_addr_byte(SEG7_ADDR | (offset << 1), pattern)
            offset += 1                     # next position
            if offset >= DISPLAY_SIZE:      # beyond physical display
                break

    def displayText(self, string, offset=0):
        """ display a text string starting on a relative position (default 0) """
        self.displaypatterns(self._encode_string(string), offset)

    def buttons(self):
        """ returns 8-bit pattern with actual button positions """
        return self._read_buttons()
